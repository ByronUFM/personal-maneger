<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Request;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',"profilePicture"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function obtenerMovimientosUsuario()
    {
        $movimientos = Movimiento::where("user_id","=",auth()->user()->id)->get();
        if(Request::has("descripcion")) {
            $movimientos = Movimiento::where("user_id","=",auth()->user()->id)->where("descripcion","=",Request::get("descripcion"))->get();
        }
        if(Request::has("descripcion") && Request::has("tipo")) {
            $movimientos = Movimiento::where("user_id","=",auth()->user()->id)
                                        ->where("descripcion","=",Request::get("descripcion"))
                                        ->where("tipo_movimiento","=",Request::get("tipo"))
                                        ->get();
        }
        return $movimientos;
    }

    public function getTotalGastado()
    {
        $totalGastado = 0;
        $movimientosDeGasto = Movimiento::where("user_id","=",auth()->user()->id)->where("tipo_movimiento","=","ND")->orWhere("tipo_movimiento","=","CQ")->get();
        foreach($movimientosDeGasto as $movimiento) {
            $totalGastado += $movimiento->cantidad;
        }
        return $totalGastado;
    }

    public function getIngresos()
    {
        $totalIngresos = 0;
        $movimientosIngresos = Movimiento::where("user_id","=",auth()->user()->id)->where("tipo_movimiento","=","NC")->get();
        foreach($movimientosIngresos as $movimiento) {
            $totalIngresos += $movimiento->cantidad;
        }
        return $totalIngresos;
    }

    public function getTotalLibre()
    {
        return $this->getIngresos() - $this->getTotalGastado();
    }
}
