<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movimiento;
use Auth;
use DB;

class MovimientosController extends Controller
{
    public function index()
    {
        $suma = 0;
        $descripciones = DB::table("movimientos")->select("descripcion")->groupBy("descripcion")->get();
        $tipos = DB::table("movimientos")->select("tipo_movimiento")->groupBy("tipo_movimiento")->get();
        $movimientos = Auth::user()->obtenerMovimientosUsuario();
        return view("movimientos.index",[
            "movimientos" => $movimientos,
            "descripciones" => $descripciones,
            "suma" => $suma,
            "tipos" => $tipos
         ]);
    }

    /**
    * Este metodo sube un archivo al servidor
    * @param : $file FileUploaded
    * @param : $dir string
    **/
    protected function uploadFile($file,$dir)
    {
        $fileName = $dir."/".str_random(30).".".$file->getClientOriginalExtension();
        $file->move($dir,$fileName);
        return url($fileName);
    }

    protected function readCsvFile($file)
    {
        $data = [];
        $handle = fopen($file, "r");
        $header = true;
        while ($csvLine = fgetcsv($handle, 1000, ","))
        {
            if ($header) {
                $header = false;
            } else {
                array_push($data,$csvLine);
            }
        }
        return $data;

    }

    protected function formatDateFromCsv($date)
    {
        $meses = [
            "ene" => "01",
            "feb" => "02",
            "mar" => "03",
            "abr" => "04",
            "may" => "05",
            "jun" => "06",
            "jul" => "07",
            "ago" => "08",
            "sep" => "09",
            "oct" => "10",
            "nov" => "11",
            "dic" => "12",
        ];
        $dateExploded = explode("-",$date);
        $dateToSave = "2016-".$meses[strtolower($dateExploded[1])]."-".$dateExploded[0];
        return $dateToSave;
    }

    public function subirArchivoCsv(Request $request)
    {
        $urlArchivoCsv = $this->uploadFile($request->file("archivoCsv"),"uploads");
        $movimientos = $this->readCsvFile($urlArchivoCsv);
        foreach($movimientos as $movimiento)
        {
            //Preparar datos para la base de datos
            $fecha = $this->formatDateFromCsv($movimiento[0]);
            if($movimiento[1] == "NC" || $movimiento[1] == "DE") {
                $cantidad = $movimiento[5];
            } else {
                $cantidad = $movimiento[4];
            }
            $movDb = new Movimiento();
            $movDb->user_id = Auth::user()->id;
            $movDb->fecha = $fecha;
            $movDb->tipo_movimiento = $movimiento[1];
            $movDb->descripcion = $movimiento[2];
            $movDb->cantidad = $cantidad;
            $movDb->save();
        }
        session()->flash("message_good","El archivo csv se ha cargado correctamente.");
        return redirect()->back();
    }
}
