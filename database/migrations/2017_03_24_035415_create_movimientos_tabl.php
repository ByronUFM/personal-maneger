<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimientosTabl extends Migration
{
    public function up()
    {
        Schema::create("movimientos",function(Blueprint $table){
            $table->increments("id");
            $table->integer("user_id")->unsigned();
            $table->date("fecha");
            $table->string("tipo_movimiento");
            $table->string("descripcion");
            $table->string("cantidad");
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists("movimientos");
    }
}
