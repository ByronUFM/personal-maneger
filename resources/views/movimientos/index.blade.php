@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Filtros </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <select class="form-control" id="descripcion">
                                <option value="">Filtrar por establecimiento</option>
                                @foreach($descripciones as $descripcion)
                                    <option value="{{ $descripcion->descripcion }}"
                                        @if($descripcion->descripcion == Request::get("descripcion")) selected @endif
                                            > {{ $descripcion->descripcion }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control" id="tipo">
                                <option value="">Filtrar por tipo de movimiento</option>
                                @foreach($tipos as $tipo)
                                <option value="{{ $tipo->tipo_movimiento }}"
                                @if($tipo->tipo_movimiento == Request::has("tipo")) selected @endif
                                > {{ $tipo->tipo_movimiento }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <hr>
                    <button id="filtrar" class="btn btn-info"> Buscar <i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"> Movimientos </div>
                <div class="panel-body">
                    @include("partials.messages")
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#cargarGastos"> Cargar movimientos por Archivo CSV <i class="fa fa-upload" aria-hidden="true"></i> </button>
                    <hr>
                    <h3>Tus movimientos</h3>
                    <table class="table table-bordered">
                        <thead>
                            <th>Accion</th>
                            <th>#</th>
                            <th>Fecha</th>
                            <th>Tipo</th>
                            <th>Descripcion</th>
                            <th>Cantidad</th>
                            <th>Balance</th>
                        </thead>
                        <tbody>
                            <?php 
                            $balance = 0;
                             ?>
                            @foreach($movimientos as $movimiento)
                            <?php 
                                if($movimiento->tipo_movimiento == "NC" || $movimiento->tipo_movimiento == "DE") {
                                    $balance += $movimiento->cantidad;
                                } else {
                                     $balance -= $movimiento->cantidad;
                                }
                                
                            ?>
                            <tr style="font-weight: bold;@if($movimiento->tipo_movimiento == "NC" || $movimiento->tipo_movimiento == "DE") background:#2ecc71;color:white; @else  background:#e74c3c;color:white; @endif">
                                <td>
                                    <input type="checkbox" class="add-value" data-cantidad="{{ $movimiento->cantidad }}" data-tipo="{{ $movimiento->tipo_movimiento }}">
                                </td>
                                <td>{{ $movimiento->id }}</td>
                                <td>{{ $movimiento->fecha }}</td>
                                <td>{{ $movimiento->tipo_movimiento }}</td>
                                <td>{{ $movimiento->descripcion }}</td>
                                <td>Q {{ number_format($movimiento->cantidad,2) }}</td>
                                <td>
                                    Q {{ number_format($balance,2) }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"> Estado </div>
                <div class="panel-body">
                    <h3>Total seleccionado: Q. <span id="mainTotal">0</span></h3>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="cargarGastos" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Carga tus gastos por archivo CSV</h4>
            </div>
            <div class="modal-body">
                <form id="cargarArchivoForm" action="{{ route("movimientos.subirArchivoCsv") }}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="archivoCsv">Archivo CSV:</label>
                        <input type="file" name="archivoCsv" class="form-control" required="true">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" onclick="document.getElementById('cargarArchivoForm').submit()">Subir <i class="fa fa-upload" aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
  </div>
</div>


@endsection

@section("js")
<script type="text/javascript">
    var btn = document.getElementById("filtrar");
    btn.addEventListener("click",function(){
        var lugar = descripcion.value;
        var tipo = document.getElementById("tipo").value;
        window.location = window.location.origin+window.location.pathname+"?descripcion="+lugar+"&tipo="+tipo;
    });
    $(".add-value").click(function(){
        var mainTotal = $("#mainTotal");
        var cantidad = $(this).data("cantidad");
        var tipo = $(this).data("tipo");
        var totalActual = parseInt(mainTotal.text());
        if(tipo == "NC" || tipo == "DE") {
            totalActual = totalActual + cantidad;
        } else {
            totalActual = totalActual - cantidad;
        }
        mainTotal.text(totalActual);
        console.log(totalActual);
    });
</script>
@endsection
