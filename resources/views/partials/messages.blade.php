@if(session()->has("message_good"))
    <div class="alert alert-success">
        <p>{{ session()->get("message_good") }}</p>
    </div>
@endif
